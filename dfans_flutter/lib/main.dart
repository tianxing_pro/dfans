import 'dart:io';
import 'package:dfans_flutter/pages/ContainerPage.dart';
import 'package:dfans_flutter/pages/common/Drag/Application.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  GlobalKey<NavigatorState> globalKey = GlobalKey<NavigatorState>();
  Application.globalKey = globalKey;
  runApp(const MyApp());
  if (Platform.isAndroid) {
    //设置Android头部的导航栏透明
    SystemUiOverlayStyle systemUiOverlayStyle =
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RestartWidget(
      child: MaterialApp(
        ///注意 一定要navigatorKey 才能在所有界面上显示
        navigatorKey: Application.globalKey,
        // theme: ThemeData(
        //     colorScheme: ColorScheme(
        //         background: Colors.white,
        //         brightness: Brightness.dark,
        //         error: DFColors.tabBg,
        //         onBackground: DFColors.tabBg)),
        home: const Scaffold(
          body: ContainerPage(),
        ),
      ),
    );
  }
}

class RestartWidget extends StatefulWidget {
  final Widget? child;

  const RestartWidget({Key? key, @required this.child})
      : assert(child != null),
        super(key: key);

  static restartApp(BuildContext context) {
    final _RestartWidgetState? state =
        context.findAncestorStateOfType<_RestartWidgetState>();
    state?.restartApp();
  }

  @override
  _RestartWidgetState createState() => _RestartWidgetState();
}

class _RestartWidgetState extends State<RestartWidget> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: key,
      child: widget.child,
    );
  }
}
