import 'package:dfans_flutter/pages/common/Comment/CommentPage.dart';
import 'package:dfans_flutter/pages/common/WebViewPage.dart';
import 'package:dfans_flutter/pages/common/ImagesPreviewPage.dart';
import 'package:dfans_flutter/pages/ContainerPage.dart';
import 'package:dfans_flutter/pages/common/YouTubePlayerPage.dart';
import 'package:dfans_flutter/pages/login/LoginPage.dart';
import 'package:dfans_flutter/pages/login/MetaMaskLoginPage.dart';
import 'package:dfans_flutter/pages/message/MessagePage.dart';
import 'package:dfans_flutter/pages/profile/ProfilePage.dart';
import 'package:dfans_flutter/pages/profile/ProfileDetailPage.dart';
import 'package:dfans_flutter/pages/search/SearchPage.dart';
import 'package:dfans_flutter/pages/search/SearchingPage.dart';
import 'package:flutter/material.dart';

class DFansRouter {
  static const homePage = 'app://';
  static const searchPage = 'app://searchPage';
  static const messagePage = 'app://messagePage';
  static const loginPage = 'app://loginPage';
  static const profilePage = 'app://profilePage';
  static const metaMaskLoginPage = 'app://metaMaskLoginPage';
  static const profileDetailPage = 'app://profileDetailPage';
  static const searchingPage = 'app://SearchingPage';

  Widget _getPage(String url, dynamic params) {
    if (url.startsWith('https://') || url.startsWith('http://')) {
      return WebViewPage(url: url);
    } else {
      switch (url) {
        case loginPage:
          return const LoginPage();
        case homePage:
          return const ContainerPage();
        case searchPage:
          return const Search();
        case messagePage:
          return Message();
        case profilePage:
          return ProfilePage(
            userName: params?['userName'],
          );
        case profileDetailPage:
          return ProfileDetailPage(
            records: params,
          );
        case metaMaskLoginPage:
          return MetaMaskLoginPage();
        case searchingPage:
          return const SearchingPage();
      }
    }
    return const ContainerPage();
  }

  DFansRouter.pushNoParams(BuildContext context, String url) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return _getPage(url, null);
    }));
  }

  DFansRouter.push(BuildContext context, String url, dynamic params) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return _getPage(url, params);
    }));
  }

  // 预览图片
  DFansRouter.previewImages(BuildContext context, List<String> imageUrls) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ImagesPreviewPage(
        imageUrls: imageUrls,
      );
    }));
  }

  // 浏览youtobe视频
  DFansRouter.videoPlayerPage(BuildContext context, String videoUrl) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return VideoPlayerPage(
        videoUrl: videoUrl,
      );
    }));
  }

  // 去评论
  DFansRouter.toComment(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return const CommentPage();
    }));
  }
}
