import 'package:flutter/material.dart';

class Constant {
  static const double MARGIN_LEFT = 15.0;
  static const double MARGIN_RIGHT = 15.0;
  static const String ASSETS_IMG = 'assets/images/';

  static const double TAB_BOTTOM = 8.0;
}
