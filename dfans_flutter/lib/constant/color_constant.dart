import 'package:flutter/material.dart';

class DFColors {
  static const Color colorRed277 = Colors.redAccent;
  static const Color colorDefaultTitle = Color.fromARGB(255, 45, 45, 45);
  static const Color colorOrigin = Color.fromARGB(255, 232, 145, 60);
  static const Color colorDetail = Color.fromARGB(196, 197, 145, 197);
  static const Color ThemeGreen = Color.fromARGB(255, 0, 189, 95);
  static const Color white = Colors.white;
  static const Color textMain = Color(0xFF18181B);
  static const Color textTime = Color(0xFF7E818B);
  static const Color tabBg = Color(0XFF767680);
  static const Color botomBar = Color(0XFF161616);
  static const Color begin_Gradient_Color = Color(0xFFFCD535);
  static const Color music_bg = Color(0XFFFDF3D7);
  static const List<Color> login_MetaMask = [
    Color(0XFFED732E),
    Color(0XFFF09035)
  ];
  static const List<Color> login_WalletConnect = [
    Color(0XFF3272F6),
    Color(0XFF4E92F7)
  ];
  static const List<Color> login_WalletName = [
    Color(0XFF554DE3),
    Color(0XFF6B64F1)
  ];
  static const Color collectBg = Color(0XFFFCD535);

  static const Color whiteTheme = Color(0XFF000000);

  static const Color blackTheme = Color(0XFF000000);
}
