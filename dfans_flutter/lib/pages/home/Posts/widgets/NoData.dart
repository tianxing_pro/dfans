import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/constant/constant.dart';
import 'package:flutter/material.dart';

class NoData extends StatelessWidget {
  final Image? image;
  final String? title;
  final String? detail;
  final void Function()? onTap;

  const NoData({Key? key, this.image, this.title, this.detail, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            image ?? Image.asset(Constant.ASSETS_IMG + 'NoData.png'),
            Padding(
                padding: const EdgeInsets.only(top: 24, bottom: 6),
                child: Text(
                  title ?? '--',
                  style: const TextStyle(
                      color: DFColors.textMain,
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                )),
            Text(detail ?? '--')
          ],
        )));
  }
}
