import 'package:dfans_flutter/constant/constant.dart';
import 'package:dfans_flutter/models/Records.dart';
import 'package:dfans_flutter/network/API.dart';
import 'package:dfans_flutter/pages/common/widgets/MediaCard.dart';
import 'package:dfans_flutter/pages/home/Posts/widgets/NoData.dart';
import 'package:dfans_flutter/rooter.dart';
import 'package:flutter/material.dart';
import 'package:dfans_flutter/pages/profile/ProfileDetailPage.dart';

class PostsPage extends StatefulWidget {
  const PostsPage({Key? key}) : super(key: key);

  @override
  State<PostsPage> createState() => _PostsPageState();
}

class _PostsPageState extends State<PostsPage> {
  static const loadingTag = "loadingTag";
  List<Records>? list = [Records(userCode: loadingTag)];
  int totalCount = 0;
  late int page = 1;
  bool loading = false;

  @override
  void initState() {
    super.initState();
    requestAPI();
  }

  void requestAPI() async {
    loading = true;
    API.getInstance().getHome(
        types: [1, 2, 3],
        pages: page,
        requestCallBack: (value) {
          loading = false;
          setState(() {
            List<Records> mergedArray =
                List<Records>.from(value?.records ?? []);
            list?.insertAll(list!.length - 1, mergedArray);
            totalCount = value?.total ?? 0;
          });
        });
  }

  void _loadMoreData() async {
    if (loading) return;
    loading = true;
    page++;
    API.getInstance().getHome(
        types: [1, 2, 3],
        pages: page,
        requestCallBack: (value) {
          loading = false;
          setState(() {
            List<Records> mergedArray =
                List<Records>.from(value?.records ?? []);
            list?.insertAll(list!.length - 1, mergedArray);
            totalCount = value?.total ?? 0;
          });
        });
  }

  // 下拉刷新
  Future<void> _refreshData() async {
    if (loading) return;
    loading = true;
    API.getInstance().getHome(
        types: [1, 2, 3],
        pages: 1,
        requestCallBack: (value) {
          loading = false;
          setState(() {
            list = value?.records;
            totalCount = value?.total ?? 0;
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    if (list == null) {
      return NoData(
          image: Image.asset(Constant.ASSETS_IMG + 'NoData.png'),
          title: "No posts",
          detail: 'Posts from your creators will show up here',
          onTap: () {
            requestAPI();
          });
    }
    return SafeArea(
        top: false,
        bottom: false,
        child: RefreshIndicator(
            onRefresh: _refreshData,
            child: ListView.builder(
                itemCount: list?.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  if (list?[index].userCode == loadingTag) {
                    if (list!.length - 1 < totalCount && !loading) {
                      //获取数据
                      _loadMoreData();
                      //加载时显示loading
                      return Container(
                        padding: const EdgeInsets.all(16.0),
                        alignment: Alignment.center,
                        child: const SizedBox(
                          width: 24.0,
                          height: 24.0,
                          child: CircularProgressIndicator(strokeWidth: 2.0),
                        ),
                      );
                    } else {
                      return Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(16.0),
                        child: const Text(
                          "没有更多了",
                          style: TextStyle(color: Colors.grey),
                        ),
                      );
                    }
                  }
                  return getCommonItem(list, index);
                })));
  }

  ///列表的普通单个item
  getCommonItem(List<Records>? items, int index) {
    if (items == null) return;
    Records item = items[index];
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(top: Constant.MARGIN_RIGHT),
      child: Expanded(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          topView(item),
          MediaCard(
            medias: item.media ?? [],
            pic: item.pic,
          ),
          centerDetail(item),
          bottomView(),
          const Divider(
            height: 1,
            color: Colors.grey,
          )
        ],
      )),
    );
  }

  // 顶部
  topView(Records? item) {
    return Padding(
        padding: const EdgeInsets.only(
          bottom: 17,
          left: Constant.MARGIN_LEFT,
          right: Constant.MARGIN_RIGHT,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            item?.pic == null
                ? const CircleAvatar(
                    radius: 25.0,
                    backgroundImage:
                        AssetImage(Constant.ASSETS_IMG + 'placeholder.png'),
                    backgroundColor: Colors.white,
                  )
                : CircleAvatar(
                    radius: 25.0,
                    backgroundImage: NetworkImage(item?.pic ?? ''),
                    backgroundColor: Colors.white,
                  ),
            // RadiusImg(
            //   imgH: 50,
            //   imgW: 50,
            //   imgUrl: item?.pic ?? '',
            //   radius: 25.0,
            // ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(
                    item?.userName ?? '--',
                    style: const TextStyle(
                        fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, top: 4),
                  child: Text(item?.domain ?? '--'),
                )
              ],
            ),
          ],
        ));
  }

  // 描述
  centerDetail(Records? item) {
    return GestureDetector(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return ProfileDetailPage(
              records: item,
            );
          }));
        },
        child: Padding(
            padding: const EdgeInsets.only(
              left: Constant.MARGIN_LEFT,
              right: Constant.MARGIN_RIGHT,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 16, bottom: 12),
                  child: Text(
                    item?.title ?? '',
                    style: const TextStyle(
                        fontSize: 15, fontWeight: FontWeight.bold),
                    maxLines: 2,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 0, top: 4),
                  child: Text(
                    item?.content ?? '',
                    maxLines: 3,
                  ),
                )
              ],
            )));
  }

  // 底部
  bottomView() {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 10, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 28),
            child: Image.asset(
              Constant.ASSETS_IMG + 'heart.png',
              width: 25.0,
              height: 25.0,
            ),
          ),
          GestureDetector(
              onTap: () {
                DFansRouter.toComment(context);
              },
              child: Padding(
                  padding: const EdgeInsets.only(right: 28),
                  child: Image.asset(
                    Constant.ASSETS_IMG + 'comment.png',
                    width: 25.0,
                    height: 25.0,
                  ))),
          Image.asset(
            Constant.ASSETS_IMG + 'icon_share.png',
            width: 25.0,
            height: 25.0,
          ),
        ],
      ),
    );
  }
}
