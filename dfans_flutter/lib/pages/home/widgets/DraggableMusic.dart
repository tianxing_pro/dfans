import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/constant/constant.dart';
import 'package:dfans_flutter/pages/common/Drag/Application.dart';
import 'package:dfans_flutter/util/screen_utils.dart';
import 'package:flutter/material.dart';

class DraggableMusic extends StatefulWidget {
  const DraggableMusic({Key? key}) : super(key: key);

  @override
  _DraggableMusicState createState() => _DraggableMusicState();
}

class _DraggableMusicState extends State<DraggableMusic> {
  List<String> list = ["1", "2", "3"];

  bool barrierDismissible = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: const EdgeInsets.all(12),
      width: ScreenUtils.screenW(context) - 16,
      decoration: BoxDecoration(
        color: DFColors.textMain.withOpacity(0.9),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(Constant.ASSETS_IMG + 'music_headset.png'),
          Container(
            padding: const EdgeInsets.only(left: 12),
            width: 100.0,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              RichText(
                  maxLines: 1,
                  text: const TextSpan(
                    text: '大王叫我来巡山',
                    style: TextStyle(fontSize: 14, overflow: TextOverflow.fade),
                  )),
              RichText(
                  maxLines: 1,
                  text: const TextSpan(
                    text: '5.12',
                    style: TextStyle(fontSize: 10, overflow: TextOverflow.fade),
                  ))
            ]),
          ),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(Constant.ASSETS_IMG + 'music_previous.png'),
              Image.asset(Constant.ASSETS_IMG + 'music_playing.png'),
              Image.asset(Constant.ASSETS_IMG + 'music_next.png'),
              GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return ListView.builder(
                              itemCount: 3,
                              itemBuilder: (context, index) {
                                return Text(list[index]);
                              });
                          // return Column(
                          //   mainAxisAlignment: MainAxisAlignment.center,
                          //   children: [
                          //     GestureDetector(
                          //       child: const Icon(IconFont.guanbi),
                          //       onTap: () {
                          //         //
                          //         setState(() {
                          //           barrierDismissible = false;
                          //         });
                          //       },
                          //     ),
                          //     ListView.builder(
                          //         itemCount: 3,
                          //         itemBuilder: (context, index) {
                          //           return Text(list[index]);
                          //         })
                          //   ],
                          // );
                        });
                  },
                  child: Image.asset(Constant.ASSETS_IMG + 'music_list.png')),
              GestureDetector(
                child: Image.asset(Constant.ASSETS_IMG + 'music_closed.png'),
                onTap: () {
                  Application.overlayEntry?.remove();
                },
              )
            ],
          ))
        ],
      ),
    );
  }
}
