import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/constant/constant.dart';
import 'package:dfans_flutter/models/Records.dart';
import 'package:dfans_flutter/network/API.dart';
import 'package:dfans_flutter/pages/home/Posts/widgets/NoData.dart';
import 'package:flutter/material.dart';

class AudioPage extends StatefulWidget {
  const AudioPage({Key? key}) : super(key: key);

  @override
  State<AudioPage> createState() => _AudioPageState();
}

class _AudioPageState extends State<AudioPage> {
  static const loadingTag = "loadingTag";
  List<Records>? list = [Records(userCode: loadingTag)];
  int totalCount = 0;
  late int page = 1;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    requestAPI();
  }

  void requestAPI() async {
    if (isLoading) return;
    isLoading = true;
    API.getInstance().getHome(
        types: [2],
        pages: page,
        requestCallBack: (value) {
          isLoading = false;
          setState(() {
            List<Records> mergedArray =
                List<Records>.from(value?.records ?? []);
            list?.insertAll(list!.length - 1, mergedArray);
            totalCount = value?.total ?? 0;
          });
        });
  }

  void _loadMoreData() async {
    requestAPI();
  }

  // 下拉刷新
  Future<void> _refreshData() async {
    if (isLoading) return;
    isLoading = true;
    API.getInstance().getHome(
        types: [2],
        pages: 1,
        requestCallBack: (value) {
          isLoading = false;
          setState(() {
            list = value?.records;
            totalCount = value?.total ?? 0;
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    if (list == null) {
      return NoData(
          image: Image.asset(Constant.ASSETS_IMG + 'NoData.png'),
          title: "No posts",
          detail: 'Posts from your creators will show up here',
          onTap: () {
            requestAPI();
          });
    }
    return SafeArea(
        top: false,
        bottom: false,
        child: RefreshIndicator(
            onRefresh: _refreshData,
            child: ListView.builder(
                itemCount: list?.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  if (list?[index].userCode == loadingTag) {
                    if (list!.length - 1 < totalCount && !isLoading) {
                      //获取数据
                      _loadMoreData();
                      //加载时显示loading
                      return Container(
                        padding: const EdgeInsets.all(16.0),
                        alignment: Alignment.center,
                        child: const SizedBox(
                          width: 24.0,
                          height: 24.0,
                          child: CircularProgressIndicator(strokeWidth: 2.0),
                        ),
                      );
                    } else {
                      return Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(16.0),
                        child: const Text(
                          "没有更多了",
                          style: TextStyle(color: Colors.grey),
                        ),
                      );
                    }
                  }
                  return getCommonItem(list, index);
                })));
  }

  ///列表的普通单个item
  getCommonItem(List<Records>? items, int index) {
    if (items == null) return;
    Records item = items[index];
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(
          left: Constant.MARGIN_LEFT,
          right: Constant.MARGIN_RIGHT,
          top: Constant.MARGIN_RIGHT,
          bottom: 10.0),
      child: Expanded(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          topView(item),
        ],
      )),
    );
  }

  // 顶部
  topView(Records? item) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 48,
          width: 48,
          child: item?.pic == null
              ? const Image(image: AssetImage(Constant.ASSETS_IMG + 'head.jpg'))
              : Image.network(item?.pic ?? ''),
        ),
        Expanded(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Text(
                'Breathe and Flow · dec 30, 2022 ',
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: DFColors.textTime),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10.0, top: 5, bottom: 5),
              child: Text('summer 2022 updates (on wac...',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: DFColors.textMain)),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Text(
                '1 HR, 50 min',
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: DFColors.textTime),
              ),
            ),
          ],
        )),
        const Image(image: AssetImage(Constant.ASSETS_IMG + 'music_stop.png'))
      ],
    );
  }
}
