import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/constant/constant.dart';
import 'package:dfans_flutter/pages/common/KeepAliveWrapper.dart';
import 'package:dfans_flutter/pages/home/Audio/AudioPage.dart';
import 'package:dfans_flutter/pages/home/Enum.dart';
import 'package:dfans_flutter/pages/home/Posts/PostsPage.dart';
import 'package:dfans_flutter/rooter.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  List<HomeThemeType> tabs = [HomeThemeType.Posts, HomeThemeType.Audio];
  int index = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // TODO: 音乐列表
  Widget _rightIcon() {
    return GestureDetector(
      onTap: () {},
      child: const Padding(
        padding: EdgeInsets.all(10),
        child: CircleAvatar(
          backgroundImage: AssetImage(Constant.ASSETS_IMG + 'yinyue.png'),
        ),
      ),
    );
  }

  // TODO: 个人主页
  Widget _leftIcon() {
    return GestureDetector(
      onTap: () {
        DFansRouter.push(
            context, DFansRouter.profilePage, {'userName': 'maoshantong'});
      },
      child: const Padding(
        padding: EdgeInsets.all(10),
        child: CircleAvatar(
          backgroundImage: AssetImage(Constant.ASSETS_IMG + 'head.jpg'),
          radius: 15.0,
        ),
      ),
    );
  }

  // appBar
  AppBar _appBar() {
    return AppBar(
        actions: [_rightIcon()],
        backgroundColor: DFColors.white,
        leadingWidth: 50,
        elevation: 0.5,
        leading: _leftIcon(),
        title: Container(
            width: 150,
            height: 40,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: DFColors.tabBg.withOpacity(0.12))),
            child: _tabBar()));
  }

  // tabBar
  TabBar _tabBar() {
    return TabBar(
      controller: _tabController,
      indicator: const BoxDecoration(),
      labelPadding: const EdgeInsets.all(0),
      indicatorWeight: 0,
      tabs: tabs
          .map((el) => Expanded(
                  child: Container(
                height: 40,
                width: 75,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                        _tabController.index == el.index ? 8 : 0),
                    color: _tabController.index == el.index
                        ? DFColors.white
                        : DFColors.tabBg.withOpacity(0.12)),
                child: Center(
                    child: Expanded(
                        child: Text(
                  el.name,
                  style: const TextStyle(color: DFColors.textMain),
                ))),
              )))
          .toList(),
      onTap: (index) {
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: TabBarView(
        controller: _tabController,
        children: tabs.map((el) {
          if (el == HomeThemeType.Posts) {
            return const KeepAliveWrapper(
              child: PostsPage(),
            );
          }
          return const KeepAliveWrapper(
            child: AudioPage(),
          );
        }).toList(),
      ),
    );
  }
}
