import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  final String? title;
  final String? url;
  final NavigationDelegate? navigationDelegate;

  const WebViewPage(
      {Key? key, this.title, this.url, this.navigationDelegate, params})
      : super(key: key);
  @override
  _WebViewPageState createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  double _progress = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.5,
          title: Text(
            widget.title ?? '',
            style: const TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
        body: Stack(
          children: [
            WebView(
              onProgress: (progress) {
                setState(() {
                  _progress = progress / 100;
                });
              },
              initialUrl: widget.url,
              javascriptMode: JavascriptMode.unrestricted,
              navigationDelegate: widget.navigationDelegate,
              // navigationDelegate: (NavigationRequest request) {
              //   // 拦截 WebView 导航请求
              //   //   // 阻止 WebView 继续加载登录回调 URL
              //   //   return NavigationDecision.prevent;
              //   // }
              //   // 允许 WebView 继续加载其他页面
              //   return NavigationDecision.navigate;
              // },
            ),
            if (_progress < 1.0)
              LinearProgressIndicator(
                value: _progress,
                backgroundColor: Colors.grey[300],
                valueColor: const AlwaysStoppedAnimation<Color>(Colors.blue),
              ),
          ],
        ));
  }
}
