import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/util/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ImagesPreviewPage extends StatelessWidget {
  final List<String> imageUrls;

  const ImagesPreviewPage({Key? key, required this.imageUrls})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: DFColors.blackTheme,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: DFColors.blackTheme,
        title: const Text('图片浏览'),
      ),
      body: CarouselSlider(
        options: CarouselOptions(height: ScreenUtils.screenH(context)),
        items: imageUrls.map((url) {
          return Image.network(url);
        }).toList(),
      ),
    );
  }
}
