import 'package:dfans_flutter/constant/constant.dart';
import 'package:dfans_flutter/models/Media.dart';
import 'package:dfans_flutter/pages/common/widgets/MusicCard.dart';
import 'package:dfans_flutter/rooter.dart';
import 'package:dfans_flutter/widgets/image/radius_img.dart';
import 'package:flutter/material.dart';

class MediaCard extends StatelessWidget {
  final List<Media> medias;
  final String? pic;

  const MediaCard({Key? key, this.pic, required this.medias}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: showMediaCard(),
      onTap: () {
        if (medias.length == 1 && medias[0].type == 2) {
          DFansRouter.videoPlayerPage(context, medias[0].url!);
        } else {
          DFansRouter.previewImages(
              context,
              medias
                  .where((element) => element.type == 1)
                  .map((e) => e.url!)
                  .toList());
        }
      },
    );
  }

  // 展示图片 +
  showMediaCard() {
    if (medias.isEmpty) return Container();
    final media = medias[0];
    if (medias.length == 1) {
      return switchItem(media, pic);
    }
    if (medias.length > 1 && medias.length <= 3) {
      return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: medias
              .map((el) => Expanded(
                  flex: 1,
                  child: RadiusImg(
                    imgUrl: media.url,
                  )))
              .toList());
    }
    return Column(
      children: [
        RadiusImg(
          imgUrl: media.url,
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: medias
                .map((el) => Expanded(
                    flex: 1,
                    child: RadiusImg(
                      imgUrl: media.url,
                    )))
                .toList()
                .sublist(1, 4))
      ],
    );
  }

  Widget switchItem(Media? media, String? pic) {
    switch (media?.type) {
      case 1:
        return RadiusImg(
          imgUrl: media?.url,
        );
      case 3:
        return Container(
          padding: const EdgeInsets.only(left: 15, right: 15),
          height: 80,
          child: Expanded(child: MusicCard(audioUrl: media?.url)),
        );
      case 2:
        // return VideoWidget(media?.url ?? '',
        //     showProgressBar: false, previewImgUrl: media?.cover ?? '');
        return Stack(
          alignment: AlignmentDirectional.centerStart,
          children: [
            RadiusImg(
              imgUrl: media?.cover,
            ),
            Align(
              alignment: Alignment.center,
              child: Image.asset(
                Constant.ASSETS_IMG + 'ic_playing.png',
                height: 55.0,
                width: 55.0,
              ),
            ),
          ],
        );
      default:
        return RadiusImg(
          imgUrl: media?.url,
        );
    }
  }
}
