import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/constant/constant.dart';
import 'package:flutter/material.dart';

class MusicCard extends StatefulWidget {
  final String? audioUrl;
  const MusicCard({Key? key, this.audioUrl}) : super(key: key);

  @override
  State<MusicCard> createState() => _MusicCardState();
}

class _MusicCardState extends State<MusicCard> {
  @override
  void initState() {
    super.initState();
    // https://juejin.cn/post/6844903911929675790
    // widget.audioUrl;
    // https://dengxiaolong.com/article/2019/07/how-to-play-audioplaxyers-in-flutter.html
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: DFColors.music_bg,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        children: [
          Image.asset(
            Constant.ASSETS_IMG + 'music_headset.png',
            fit: BoxFit.contain,
            height: 80,
          ),
          Expanded(
              child: Padding(
                  padding: const EdgeInsets.only(
                      right: Constant.MARGIN_LEFT, left: Constant.MARGIN_LEFT),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Image.asset(Constant.ASSETS_IMG + 'music_stop_black.png'),
                      Text(
                        '03:13',
                        style: TextStyle(
                          fontSize: 14,
                          color: DFColors.textTime.withOpacity(0.6),
                        ),
                      ),
                      Image.asset(
                          Constant.ASSETS_IMG + 'music_wave_pattern.png'),
                      Image.asset(Constant.ASSETS_IMG + 'music_add.png'),
                    ],
                  )))
        ],
      ),
    );
  }
}
