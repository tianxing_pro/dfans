import 'package:flutter/material.dart';

class CommentPage extends StatefulWidget {
  const CommentPage({Key? key}) : super(key: key);

  @override
  _CommentPageState createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  final TextEditingController _textEditingController = TextEditingController();
  final FocusNode _focusNode = FocusNode();

  @override
  void dispose() {
    _textEditingController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  void _openKeyboard() {
    FocusScope.of(context).requestFocus(_focusNode);
  }

  void _submitComment() {
    String comment = _textEditingController.text;
    // 处理提交评论的逻辑
    print('提交评论: $comment');
    _textEditingController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('评论'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: _textEditingController,
              focusNode: _focusNode,
              decoration: const InputDecoration(
                hintText: '请输入评论',
              ),
              maxLines: null,
              keyboardType: TextInputType.multiline,
            ),
            const SizedBox(height: 16.0),
            TextButton(
              onPressed: _openKeyboard,
              child: const Text('编辑'),
            ),
            TextButton(
              onPressed: _submitComment,
              child: const Text('提交评论'),
            ),
          ],
        ),
      ),
    );
  }
}
