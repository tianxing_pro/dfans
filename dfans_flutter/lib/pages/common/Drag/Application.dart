import 'package:flutter/cupertino.dart';

class Application {
  static Application? _instance;
  // 具体初始化代码
  Application._() {}
  static Application sharedInstance() {
    _instance ??= Application._();
    return _instance!;
  }

  ///应用全局 ke
  static GlobalKey<NavigatorState>? globalKey;
  static OverlayEntry? overlayEntry;
}
