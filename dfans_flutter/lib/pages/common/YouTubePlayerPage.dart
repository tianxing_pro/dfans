import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:fijkplayer/fijkplayer.dart';

// 可播放 YouTube + bilibili 视频
class VideoPlayerPage extends StatefulWidget {
  final String videoUrl;

  const VideoPlayerPage({Key? key, @required required this.videoUrl})
      : super(key: key);

  @override
  _VideoPlayerPageState createState() => _VideoPlayerPageState();
}

class _VideoPlayerPageState extends State<VideoPlayerPage> {
  final FijkPlayer player = FijkPlayer();
  String? videoId;

  _VideoPlayerPageState();

  @override
  void initState() {
    super.initState();
    videoId = YoutubePlayer.convertUrlToId(widget.videoUrl);
    if (videoId != null) {
      player.setDataSource(widget.videoUrl, autoPlay: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: DFColors.blackTheme,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: DFColors.blackTheme,
          title: const Text('视频播放'),
        ),
        body: Container(
          alignment: Alignment.center,
          child: getVideoView(),
        ));
  }

  Widget getVideoView() {
    if (videoId != null) {
      return YoutubePlayer(
        controller: YoutubePlayerController(
          initialVideoId: videoId!,
          flags: const YoutubePlayerFlags(
            autoPlay: true,
            mute: false,
          ),
        ),
        showVideoProgressIndicator: true,
      );
    }
    return FijkView(
      player: player,
    );
  }

  @override
  void dispose() {
    super.dispose();
    player.release();
  }
}
