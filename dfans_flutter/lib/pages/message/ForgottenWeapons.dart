import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/constant/constant.dart';
import 'package:flutter/material.dart';

class ForgottenWeapons extends StatefulWidget {
  const ForgottenWeapons({Key? key}) : super(key: key);

  @override
  State<ForgottenWeapons> createState() => _ForgottenWeaponsState();
}

class _ForgottenWeaponsState extends State<ForgottenWeapons> {
  List<String> items = ['Item 1', 'Item 2'];

  Widget renderList() {
    return ListView.builder(
        itemCount: items.length,
        reverse: true,
        itemBuilder: (BuildContext context, int index) {
          return topView();
        });
  }

  // 顶部
  topView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
            height: 40,
            width: 40,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: const Image(
                  image: AssetImage(Constant.ASSETS_IMG + 'head.jpg')),
            )),
        Expanded(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Padding(
              padding: EdgeInsets.only(left: 10.0, top: 5, bottom: 5),
              child: Text('Matthew Chaim podcast',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: DFColors.textMain)),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Text(
                'Thanks so much for responding to my other message.',
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: DFColors.textTime),
              ),
            ),
          ],
        )),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: DFColors.white,
        leadingWidth: 50,
        elevation: 0.5,
        leading: GestureDetector(
          child: Image.asset(
            Constant.ASSETS_IMG + 'back.png',
            width: 50.0,
            height: 50.0,
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
        title: const Text(
          'Forgotten Weapons',
          style: TextStyle(
              color: DFColors.textMain,
              fontSize: 17,
              fontWeight: FontWeight.bold),
        ),
      ),
      body: renderList(),
    );
  }
}
