import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/network/API.dart';
import 'package:dfans_flutter/pages/common/Drag/Application.dart';
import 'package:dfans_flutter/pages/home/HomePage.dart';
import 'package:dfans_flutter/pages/home/widgets/DraggableMusic.dart';
import 'package:dfans_flutter/pages/message/MessagePage.dart';
import 'package:dfans_flutter/pages/search/SearchPage.dart';
import 'package:dfans_flutter/util/screen_utils.dart';
import 'package:dfans_flutter/widgets/DFToast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

///这个页面是作为整个APP的最外层的容器，以Tab为基础控制每个item的显示与隐藏
class ContainerPage extends StatefulWidget {
  const ContainerPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ContainerPageState();
  }
}

class _Item {
  String name, activeIcon, normalIcon;

  _Item(this.name, this.activeIcon, this.normalIcon);
}

class _ContainerPageState extends State<ContainerPage> {
  List<Widget>? pages;
  final defaultItemColor = const Color.fromARGB(255, 125, 125, 125);
  final itemNames = [
    _Item('', 'assets/images/home_active.png', 'assets/images/home_normal.png'),
    _Item('', 'assets/images/message_active.png',
        'assets/images/message_normal.png'),
    _Item('', 'assets/images/search_active.png',
        'assets/images/search_normal.png'),
  ];
  int _selectIndex = 0;
  List<BottomNavigationBarItem>? itemList;
  // 浮动图层
  OverlayEntry? overlayEntry;

  @override
  void initState() {
    super.initState();
    pages ??= [const Home(), const Search(), const Message()];
    itemList ??= itemNames
        .map((item) => BottomNavigationBarItem(
            icon: Image.asset(
              item.normalIcon,
              width: 40.0,
              height: 40.0,
            ),
            label: item.name,
            activeIcon:
                Image.asset(item.activeIcon, width: 30.0, height: 30.0)))
        .toList();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      /// WidgetsBinding.instance.addPostFrameCallback
      /// 这个作用是界面绘制完成的监听回调  必须在绘制完成后添加OverlayEntry
      addOverlayEntry(8, 64 + ScreenUtils.getBottomBarH(context));
    });
    setMethodCallHandler();
  }

  static const platform = MethodChannel('dfans.flutter.bridge');
  setMethodCallHandler() {
// 注册方法处理程序
    platform.setMethodCallHandler((call) async {
      if (call.method == 'loginWithAccount') {
        // 在此处实现来自原生代码的方法
        // 可以执行相关操作并返回结果给 Flutter
        print(' call.arguments ---- ${call.arguments}');
        checkLogin(
            account: call.arguments?['account'],
            signature: call.arguments?['signature']);
        return 'Result from native method';
      } else {
        return null;
      }
    });
  }

  // 验证是否登录
  checkLogin({String? account, String? signature}) async {
    API.getInstance().fetchLogin(account, signature, (value) {
      print(' value --- $value');
      DFToast.showToast('登录成功');
    });
  }

  // 添加实体
  addOverlayEntry(double left, double bottom) {
    Application.overlayEntry?.remove();
    overlayEntry = OverlayEntry(
        builder: (BuildContext context) => Positioned(
              left: left,
              bottom: bottom,

              ///拖动结束
              child: Draggable(
                  onDragEnd: (DraggableDetails details) {
                    // addOverlayEntry(details.offset.dx, details.offset.dy);
                  },
                  feedback: const DraggableMusic(),
                  child: const DraggableMusic(),
                  childWhenDragging: Container()),
            ));
    Application.overlayEntry = overlayEntry;
    Application.globalKey?.currentState?.overlay?.insert(overlayEntry!);
  }

//Stack（层叠布局）+Offstage组合,解决状态被重置的问题
  Widget _getPagesWidget(int index) {
    return Offstage(
      offstage: _selectIndex != index,
      child: TickerMode(
        enabled: _selectIndex == index,
        child: (pages ?? [])[index],
      ),
    );
  }

  @override
  void didUpdateWidget(ContainerPage oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _getPagesWidget(0),
          _getPagesWidget(1),
          _getPagesWidget(2),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: itemList ?? [],
        onTap: (int index) {
          ///这里根据点击的index来显示，非index的page均隐藏
          setState(() {
            _selectIndex = index;
          });
        },
        iconSize: 30,
        currentIndex: _selectIndex,
        backgroundColor: DFColors.botomBar.withOpacity(0.9),
        type: BottomNavigationBarType.fixed,
        showSelectedLabels: false,
      ),
    );
  }
}
