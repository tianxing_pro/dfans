import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/pages/ContainerPage.dart';
import 'package:dfans_flutter/pages/login/LoginProvider.dart';
import 'package:dfans_flutter/rooter.dart';
import 'package:flutter/material.dart';
import '../../constant/constant.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LoginState();
  }
}

class _LoginState extends State<LoginPage> {
  var container = const ContainerPage();
  final loginProvider = LoginProvider();

  @override
  void initState() {
    super.initState();
  }

  _onPressed() {
    DFansRouter.pushNoParams(context, DFansRouter.homePage);
    loginProvider.connectToEthereum();
    // DFansRouter.pushNoParams(context, DFansRouter.metaMaskLoginPage);
  }

  @override
  Widget build(BuildContext context) {
    return _container();
  }

  Widget _container() {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            DFColors.begin_Gradient_Color.withOpacity(0.1),
            DFColors.begin_Gradient_Color.withOpacity(0)
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Padding(
          padding: const EdgeInsets.only(left: 24, right: 24),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Image(
                    image: AssetImage(Constant.ASSETS_IMG + 'theme.png')),
                GestureDetector(
                    onTap: () {
                      _onPressed();
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(top: 80, bottom: 20),
                      child: _card(DFColors.login_MetaMask, "MetaMask"),
                    )),
                GestureDetector(
                    onTap: () {
                      _onPressed();
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child:
                          _card(DFColors.login_WalletConnect, "WalletConnect"),
                    )),
              ],
            ),
          )),
    );
  }

  Widget _card(List<Color> colors, String iconName) {
    return ClipRRect(
        borderRadius: BorderRadius.circular(50.0),
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: colors,
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(12),
                child: Image(
                    image: AssetImage(Constant.ASSETS_IMG + iconName + '.png')),
              ),
              Text(
                iconName,
                style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: DFColors.white),
              ),
              Image(
                  image:
                      AssetImage(Constant.ASSETS_IMG + iconName + '_Alpha.png'))
            ],
          ),
        ));
  }
}
