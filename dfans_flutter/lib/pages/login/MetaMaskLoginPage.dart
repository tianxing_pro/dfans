import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MetaMaskLoginPage extends StatefulWidget {
  @override
  _MetaMaskLoginPageState createState() => _MetaMaskLoginPageState();
}

class _MetaMaskLoginPageState extends State<MetaMaskLoginPage> {
  double _progress = 0;
  final String metaMaskLoginPageUrl = "http://dfans.myslash.co:8086/";
  // 'https://metamask.app.link/dapp/dfans-metamash.com';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.5,
          title: const Text(
            'MetaMask Login',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
        body: Stack(
          children: [
            WebView(
              onProgress: (progress) {
                setState(() {
                  _progress = progress / 100;
                });
              },
              initialUrl: metaMaskLoginPageUrl,
              javascriptMode: JavascriptMode.unrestricted,
              // navigationDelegate: (NavigationRequest request) {
              //   // 拦截 WebView 导航请求，获取 MetaMask 登录结果
              //   // if (request.url
              //   //     .startsWith('your-app-scheme://metamask-login-result')) {
              //   //   // 提取登录结果并处理
              //   //   final loginResult = request.url
              //   //       .replaceAll('your-app-scheme://metamask-login-result?', '');
              //   //   handleMetaMaskLoginResult(loginResult);

              //   //   // 阻止 WebView 继续加载登录回调 URL
              //   //   return NavigationDecision.prevent;
              //   // }
              //   // 允许 WebView 继续加载其他页面
              //   return NavigationDecision.navigate;
              // },
            ),
            if (_progress < 1.0)
              LinearProgressIndicator(
                value: _progress,
                backgroundColor: Colors.grey[300],
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
              ),
          ],
        ));
  }

  void handleMetaMaskLoginResult(String result) {
    // 处理 MetaMask 登录结果
    // 解析结果并执行相应的操作
    print('MetaMask Login Result: $result');
  }
}
