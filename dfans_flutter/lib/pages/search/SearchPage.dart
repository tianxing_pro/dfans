import 'package:dfans_flutter/constant/color_constant.dart';
import 'package:dfans_flutter/pages/common/KeepAliveWrapper.dart';
import 'package:dfans_flutter/pages/home/Posts/widgets/NoData.dart';
import 'package:dfans_flutter/rooter.dart';
import 'package:flutter/material.dart';
import '../../constant/constant.dart';

class Search extends StatefulWidget {
  final String searchHintContent;

  ///搜索框中的默认显示内容
  const Search({Key? key, this.searchHintContent = ''}) : super(key: key);
  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> with SingleTickerProviderStateMixin {
  bool showLoading = false;
  late TabController _tabController;
  List<String> tabs = ['MyCollected', 'Following'];
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
  }

  Widget _leftIcon() {
    return GestureDetector(
      onTap: () {
        DFansRouter.push(
            context, DFansRouter.profilePage, {'userName': 'maoshantong'});
      },
      child: const Padding(
        padding: EdgeInsets.all(10),
        child: CircleAvatar(
          backgroundImage: AssetImage(Constant.ASSETS_IMG + 'head.jpg'),
          radius: 15.0,
        ),
      ),
    );
  }

  Widget _rightIcon() {
    return GestureDetector(
      onTap: () {
        DFansRouter.push(context, DFansRouter.searchingPage, {});
      },
      child: const Center(
        child: Padding(
            padding: EdgeInsets.only(right: 12),
            child: Text(
              "Search",
              style: TextStyle(fontSize: 15, color: DFColors.blackTheme),
            )),
      ),
    );
  }

  // tabBar
  TabBar _tabBar() {
    return TabBar(
      controller: _tabController,
      indicatorSize: TabBarIndicatorSize.label,
      labelPadding: const EdgeInsets.all(0),
      tabs: tabs
          .map((el) => Tab(
                child: Text(
                  el,
                  style: const TextStyle(color: DFColors.textMain),
                ),
              ))
          .toList(),
      onTap: (index) {
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [_rightIcon()],
        backgroundColor: DFColors.white,
        leadingWidth: 50,
        elevation: 0.5,
        title: Text(
          'Find creators',
          style: getStyle(DFColors.textMain, 17, bold: true),
        ),
        leading: _leftIcon(),
        bottom: _tabBar(),
      ),
      body: TabBarView(
        controller: _tabController,
        children: tabs.map((el) {
          if (el == "MyCollected") {
            return KeepAliveWrapper(
              child: NoData(
                  image: Image.asset(Constant.ASSETS_IMG + 'NoData.png'),
                  title: "No My Collected",
                  detail: 'your Collected will show up here',
                  onTap: () {
                    // requestAPI();
                  }),
            );
          }
          return KeepAliveWrapper(
            child: NoData(
                image: Image.asset(Constant.ASSETS_IMG + 'NoData.png'),
                title: "No following",
                detail: 'your following will show up here',
                onTap: () {
                  // requestAPI();
                }),
          );
        }).toList(),
      ),
    );
  }

  @override
  void dispose() {
    // 释放资源
    _tabController.dispose();
    super.dispose();
  }

  TextStyle getStyle(Color color, double fontSize, {bool bold = false}) {
    return TextStyle(
        color: color,
        fontSize: fontSize,
        fontWeight: bold ? FontWeight.bold : FontWeight.normal);
  }
}
