import 'package:flutter/widgets.dart';

class IconFont {
  IconFont();

  static const IconData genghuanwuliao =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData fanbai_pandianrenwu =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xuanze = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData mianxingzhankai =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData mianxingshouqi =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData heidi_qiehuan =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData heidi_duoxuan =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData PDA = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xiayiji = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xiaji = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xuanzejieshou =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData guige = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData biaoshima = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData cangku1 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData piliangtianjia =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData cangwei = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData buzhoutiao_yiwancheng =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData a_gengduoshuban =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData biaoqian = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shuju1 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData qiantao = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shuju = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData dayin = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shebei1 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData banxuan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData daojishi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData tuxingyanzheng =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData bianjijilu = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shangchuanfujian =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData TXT = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData EXCEl = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData MP4 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData Word = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData huoqu = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xuanzepici = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData weizhiwenjian =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shibaishenqing =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData jihuazhihangren =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData dangqianzhihangren =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData PNG = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData JPG = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData PDF = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData bianji = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData tongdao = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData yun = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData panding = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData jinji2 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData jinji = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData zanwushuju = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData duoxuan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData heji1 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData dianziqianming =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData tiaobo = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData paixu = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData huiche = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xinxiquan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData a_ = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData ruku = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shujushuliang =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData erweima = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData fujian = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData weizhibeifen = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shangchuanwenjian1 =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData sousuotianjia =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData saomatianjia = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData chuku = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData heihuruanjianfuwuxieyi =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shebeihao = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData zhanghaoyuanquan =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xitongyuyan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData saomafangshi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData banbenxinxi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData fuzhi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData gerenxinxibaohuzhengce =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData jiantoujinru = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xunhuan1 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData qiehuan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData chakanwenjian =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xunhuan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shouqi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData zhankai = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData chakan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData wenjianjia_guanbi_hui =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData wenjianjia_dakai_hui =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData wenjianjia_dakai1 =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData wenjianjia_guanbi1 =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData fanhui = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xinzengyiye = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData lingqu = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData sheweiyangben =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData yangbenjilu = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData diantong_guan =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xiangce = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData diantong_kai = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shoudongshuru =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xiaoxi_xuanzhong1 =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData gongneng_xuanzhong1 =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shouye_xuanzhong =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData zhijian = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData banbengengxin =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shebei = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData tongzhixiaoxi =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shengchan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xitonggonggao =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData yichangtixing =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData wuliao = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData gongyi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData cangku = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xing = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shezhi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shangchuanwenjian =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData quanbu = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData tuozhuai = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData gengduo = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shanchu1 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData jieshu = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData chakanxiangqing =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData yulan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData tianjia = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData zhongzhi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData duoxuan21 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shaixuan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData renwu = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData gongneng = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shouye = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData renwu_xuanzhong =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xiaoxi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData wode_xuanzhong =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData wode = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData yixuan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData weixuan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData weixuan_shixiao =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData yixuan_shixiao =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData duoxuan_shixiao =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData gongchang = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData mima = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData geren = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shouji = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData saoma = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData a_mianxingshanchu =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData a_mianxingguanbi =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData guanbi2 = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData a_mianxingxiangji =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData a_mianxingshijian =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData laba = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xianshi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData yincang = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shijian = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData rili = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shuangyou = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shuangzuo = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xiangshang = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xiangzuo = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xiangxia = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xiangyou = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData jingshi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData tishi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData huakuaituozhuai =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData jingyin = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shengyinda = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shengyinxiao = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shuidizhixiangyou =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData gou = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData a_mianxinggouxuan =
      IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData gouxuan = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData jiazai = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shanchu = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData guanbi = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData sousuo = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData _ = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData icon_test = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData shang = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData you = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData zuo = IconData(0xE620, fontFamily: 'Iconfont');
  static const IconData xia = IconData(0xE620, fontFamily: 'Iconfont');
}
