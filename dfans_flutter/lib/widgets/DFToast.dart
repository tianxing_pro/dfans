import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class DFToast {
  static showToast(String msg) {
    Fluttertoast.showToast(
      msg: msg, // Toast 的内容
      toastLength: Toast
          .LENGTH_SHORT, // Toast 的显示时长，可以是 Toast.LENGTH_SHORT（短时显示）或 Toast.LENGTH_LONG（长时显示）
      gravity: ToastGravity
          .CENTER, // Toast 的显示位置，可以是 ToastGravity.TOP、ToastGravity.CENTER 或 ToastGravity.BOTTOM
      timeInSecForIosWeb: 2, // iOS 和 Web 平台上的显示时长，单位为秒
      backgroundColor: Colors.black, // Toast 的背景颜色
      textColor: Colors.white, // Toast 的文本颜色
      fontSize: 16.0, // Toast 的文本字体大小
    );
  }
}
