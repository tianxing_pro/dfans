import 'package:dfans_flutter/constant/constant.dart';
import 'package:flutter/material.dart';

class RadiusImg extends StatelessWidget {
  String? imgUrl;
  double? imgW;
  double? imgH;
  Color? shadowColor;
  double? elevation;
  double? radius = 6.0;
  RoundedRectangleBorder? shape;

  RadiusImg(
      {Key? key,
      this.imgUrl,
      this.imgW,
      this.imgH,
      this.shadowColor = Colors.transparent,
      this.elevation,
      this.radius,
      this.shape})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (imgUrl == null) return Container();
    return Card(
      shape: shape ??
          RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(radius ?? 0)),
          ),
      color: shadowColor,
      clipBehavior: Clip.antiAlias,
      elevation: elevation == null ? 0.0 : 5.0,
      child: imgW == null
          ? FadeInImage.assetNetwork(
              height: imgH,
              fit: BoxFit.cover,
              placeholder: Constant.ASSETS_IMG + 'placeholder.png',
              image: imgUrl!,
            )
          : FadeInImage.assetNetwork(
              image: imgUrl!,
              width: imgW,
              height: imgH,
              placeholder: Constant.ASSETS_IMG + 'placeholder.png',
              fit: imgH == null ? BoxFit.contain : BoxFit.cover,
            ),
    );
  }
}
