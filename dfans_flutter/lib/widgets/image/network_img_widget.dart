import 'package:dfans_flutter/constant/constant.dart';
import 'package:flutter/material.dart';

class NetworkImgWidget extends StatefulWidget {
  final String? imgUrl;
  final String? placeHolderAsset;

  const NetworkImgWidget({Key? key, this.placeHolderAsset, this.imgUrl})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _NetworkImgWidgetState();
  }
}

class _NetworkImgWidgetState extends State<NetworkImgWidget> {
  late Image img, netImg;

  @override
  void initState() {
    super.initState();
    img = Image.asset(
        widget.placeHolderAsset ?? Constant.ASSETS_IMG + 'placeholder.png');
    try {
      netImg = Image.network(widget.imgUrl ?? '');
    } on Exception catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[img, netImg],
    );
  }
}
