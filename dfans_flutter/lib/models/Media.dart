class Media {
  Media({
    this.url,
    this.name,
    this.type,
    this.cover,
    this.vertical,
  });

  Media.fromJson(dynamic json) {
    url = json?['url'];
    name = json?['name'];
    type = json?['type'];
    cover = json?['cover'];
    vertical = json?['vertical'];
  }
  String? url;
  String? name;
  int? type;
  String? cover;
  int? vertical;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = url;
    map['name'] = name;
    map['type'] = type;
    map['cover'] = cover;
    map['vertical'] = vertical;
    return map;
  }
}
