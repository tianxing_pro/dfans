import 'Records.dart';

class PostsData {
  PostsData({
    this.records,
    this.total,
    this.size,
    this.current,
    this.optimizeCountSql,
    this.hitCount,
    this.countId,
    this.maxLimit,
    this.searchCount,
    this.pages,
  });

  PostsData.fromJson(dynamic json) {
    if (json?['records'] != null) {
      records = [];
      json?['records'].forEach((v) {
        records?.add(Records.fromJson(v));
      });
    }
    total = json?['total'];
    size = json?['size'];
    current = json?['current'];
    optimizeCountSql = json?['optimizeCountSql'];
    hitCount = json?['hitCount'];
    countId = json?['countId'];
    maxLimit = json?['maxLimit'];
    searchCount = json?['searchCount'];
    pages = json?['pages'];
  }
  List<Records>? records;
  int? total;
  int? size;
  int? current;
  bool? optimizeCountSql;
  bool? hitCount;
  int? countId;
  int? maxLimit;
  bool? searchCount;
  int? pages;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (records != null) {
      map['records'] = records?.map((v) => v.toJson()).toList();
    }
    map['total'] = total;
    map['size'] = size;
    map['current'] = current;
    map['optimizeCountSql'] = optimizeCountSql;
    map['hitCount'] = hitCount;
    map['countId'] = countId;
    map['maxLimit'] = maxLimit;
    map['searchCount'] = searchCount;
    map['pages'] = pages;
    return map;
  }
}
