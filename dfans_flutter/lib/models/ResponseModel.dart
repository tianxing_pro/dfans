import 'PostsData.dart';

class ResponseModel {
  ResponseModel({
    this.code,
    this.msg,
    this.data,
    this.success,
  });

  ResponseModel.fromJson(dynamic json) {
    code = json?['code'];
    msg = json?['msg'];
    data = json?['data'] != null ? PostsData.fromJson(json?['data']) : null;
    success = json?['success'];
  }
  int? code;
  String? msg;
  PostsData? data;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = code;
    map['msg'] = msg;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['success'] = success;
    return map;
  }
}
