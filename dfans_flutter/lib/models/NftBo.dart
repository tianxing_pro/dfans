class NftBo {
  NftBo({
    this.tokenId,
    this.price,
  });

  NftBo.fromJson(dynamic json) {
    tokenId = json['tokenId'];
    price = json['price'];
  }
  String? tokenId;
  String? price;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['tokenId'] = tokenId;
    map['price'] = price;
    return map;
  }
}
