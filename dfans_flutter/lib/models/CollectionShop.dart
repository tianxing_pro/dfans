import 'NftBo.dart';
import 'BlogMediaTypeCount.dart';

class CollectionShop {
  CollectionShop({
    this.id,
    this.link,
    this.contract,
    this.name,
    this.chain,
    this.cover,
    this.contractType,
    this.nftCount,
    this.founding,
    this.note,
    this.totalSold,
    this.nftBO,
    this.publicSaleStartingTime,
    this.publicSaleMax,
    this.publicSaleMintPrice,
    // this.presaleInfoList,
    this.revenueRate,
    this.blogMediaTypeCount,
    this.domain,
    this.deployStatus,
    this.failReason,
  });

  CollectionShop.fromJson(dynamic json) {
    id = json['id'];
    link = json['link'];
    contract = json['contract'];
    name = json['name'];
    chain = json['chain'];
    cover = json['cover'];
    contractType = json['contractType'];
    nftCount = json['nftCount'];
    founding = json['founding'];
    note = json['note'];
    totalSold = json['totalSold'];
    nftBO = json['nftBO'] != null ? NftBo.fromJson(json['nftBO']) : null;
    publicSaleStartingTime = json['publicSaleStartingTime'];
    publicSaleMax = json['publicSaleMax'];
    publicSaleMintPrice = json['publicSaleMintPrice'];
    // if (json['presaleInfoList'] != null) {
    //   presaleInfoList = [];
    //   json['presaleInfoList'].forEach((v) {
    //     presaleInfoList?.add(Dynami.fromJson(v));
    //   });
    // }
    revenueRate = json['revenueRate'];
    if (json['blogMediaTypeCount'] != null) {
      blogMediaTypeCount = [];
      json['blogMediaTypeCount'].forEach((v) {
        blogMediaTypeCount?.add(BlogMediaTypeCount.fromJson(v));
      });
    }
    domain = json['domain'];
    deployStatus = json['deployStatus'];
    failReason = json['failReason'];
  }
  int? id;
  dynamic link;
  String? contract;
  String? name;
  int? chain;
  String? cover;
  String? contractType;
  int? nftCount;
  int? founding;
  String? note;
  int? totalSold;
  NftBo? nftBO;
  int? publicSaleStartingTime;
  int? publicSaleMax;
  double? publicSaleMintPrice;
  // List<dynamic>? presaleInfoList;
  double? revenueRate;
  List<BlogMediaTypeCount>? blogMediaTypeCount;
  String? domain;
  int? deployStatus;
  dynamic failReason;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['link'] = link;
    map['contract'] = contract;
    map['name'] = name;
    map['chain'] = chain;
    map['cover'] = cover;
    map['contractType'] = contractType;
    map['nftCount'] = nftCount;
    map['founding'] = founding;
    map['note'] = note;
    map['totalSold'] = totalSold;
    if (nftBO != null) {
      map['nftBO'] = nftBO?.toJson();
    }
    map['publicSaleStartingTime'] = publicSaleStartingTime;
    map['publicSaleMax'] = publicSaleMax;
    map['publicSaleMintPrice'] = publicSaleMintPrice;
    // if (presaleInfoList != null) {
    //   map['presaleInfoList'] = presaleInfoList?.map((v) => v.toJson()).toList();
    // }
    map['revenueRate'] = revenueRate;
    if (blogMediaTypeCount != null) {
      map['blogMediaTypeCount'] =
          blogMediaTypeCount?.map((v) => v.toJson()).toList();
    }
    map['domain'] = domain;
    map['deployStatus'] = deployStatus;
    map['failReason'] = failReason;
    return map;
  }
}
