class Contact {
  Contact({
    this.url,
    this.type,
    this.phone,
    this.account,
  });

  Contact.fromJson(dynamic json) {
    url = json['url'];
    type = json['type'];
    phone = json['phone'];
    account = json['account'];
  }
  String? url;
  int? type;
  dynamic phone;
  dynamic account;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = url;
    map['type'] = type;
    map['phone'] = phone;
    map['account'] = account;
    return map;
  }
}
