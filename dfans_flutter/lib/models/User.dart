import 'Contact.dart';

class User {
  User({
    this.address,
    this.userName,
    this.userCode,
    this.shopCode,
    this.background,
    this.contact,
    this.pic,
    this.selfIntro,
    this.nfts,
    this.totalSold,
    this.total,
    this.owners,
    this.followerNumber,
    this.following,
    this.collected,
    this.totalInvest,
    this.imageCount,
    this.videoCount,
    this.privateChatId,
  });

  User.fromJson(dynamic json) {
    address = json['address'];
    userName = json['userName'];
    userCode = json['userCode'];
    shopCode = json['shopCode'];
    background = json['background'];
    if (json['contact'] != null) {
      contact = [];
      json['contact'].forEach((v) {
        contact?.add(Contact.fromJson(v));
      });
    }
    pic = json['pic'];
    selfIntro = json['selfIntro'];
    nfts = json['nfts'];
    totalSold = json['totalSold'];
    total = json['total'];
    owners = json['owners'];
    followerNumber = json['followerNumber'];
    following = json['following'];
    collected = json['collected'];
    totalInvest = json['totalInvest'];
    imageCount = json['imageCount'];
    videoCount = json['videoCount'];
    privateChatId = json['privateChatId'];
  }
  String? address;
  String? userName;
  String? userCode;
  String? shopCode;
  dynamic background;
  List<Contact>? contact;
  String? pic;
  dynamic selfIntro;
  int? nfts;
  int? totalSold;
  String? total;
  int? owners;
  int? followerNumber;
  dynamic following;
  dynamic collected;
  dynamic totalInvest;
  int? imageCount;
  int? videoCount;
  dynamic privateChatId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['address'] = address;
    map['userName'] = userName;
    map['userCode'] = userCode;
    map['shopCode'] = shopCode;
    map['background'] = background;
    if (contact != null) {
      map['contact'] = contact?.map((v) => v.toJson()).toList();
    }
    map['pic'] = pic;
    map['selfIntro'] = selfIntro;
    map['nfts'] = nfts;
    map['totalSold'] = totalSold;
    map['total'] = total;
    map['owners'] = owners;
    map['followerNumber'] = followerNumber;
    map['following'] = following;
    map['collected'] = collected;
    map['totalInvest'] = totalInvest;
    map['imageCount'] = imageCount;
    map['videoCount'] = videoCount;
    map['privateChatId'] = privateChatId;
    return map;
  }
}
