class BlogTypeCount {
  BlogTypeCount({
    this.type,
    this.count,
  });

  BlogTypeCount.fromJson(dynamic json) {
    type = json?['type'];
    count = json?['count'];
  }
  int? type;
  int? count;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = type;
    map['count'] = count;
    return map;
  }
}
