import 'Media.dart';
import 'BlogTypeCount.dart';

class Records {
  Records({
    this.userCode,
    this.shopCode,
    this.id,
    this.commentCount,
    this.status,
    this.media,
    this.favorite,
    this.pic,
    this.publishTime,
    this.domain,
    this.totalAmount,
    this.userName,
    this.country,
    this.title,
    this.pageView,
    this.content,
    this.thumbUp,
    this.blogType,
    this.blogTypeCount,
    this.top,
    this.forceUnLock,
  });

  Records.fromJson(dynamic json) {
    userCode = json?['userCode'];
    shopCode = json?['shopCode'];
    id = json?['id'];
    commentCount = json?['commentCount'];
    status = json?['status'];
    if (json?['media'] != null) {
      media = [];
      json?['media'].forEach((v) {
        media?.add(Media.fromJson(v));
      });
    }
    favorite = json?['favorite'];
    pic = json?['pic'];
    publishTime = json?['publishTime'];
    domain = json?['domain'];
    totalAmount = json?['totalAmount'];
    userName = json?['userName'];
    country = json?['country'];
    title = json?['title'];
    pageView = json?['pageView'];
    content = json?['content'];
    thumbUp = json?['thumbUp'];
    blogType = json?['blogType'];
    if (json?['blogTypeCount'] != null) {
      blogTypeCount = [];
      json?['blogTypeCount'].forEach((v) {
        blogTypeCount?.add(BlogTypeCount.fromJson(v));
      });
    }
    top = json?['top'];
    forceUnLock = json?['forceUnLock'];
  }
  String? userCode;
  String? shopCode;
  int? id;
  int? commentCount;
  int? status;
  List<Media>? media;
  int? favorite;
  dynamic? pic;
  int? publishTime;
  String? domain;
  int? totalAmount;
  String? userName;
  String? country;
  String? title;
  int? pageView;
  String? content;
  int? thumbUp;
  int? blogType;
  List<BlogTypeCount>? blogTypeCount;
  int? top;
  bool? forceUnLock;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['userCode'] = userCode;
    map['shopCode'] = shopCode;
    map['id'] = id;
    map['commentCount'] = commentCount;
    map['status'] = status;
    if (media != null) {
      map['media'] = media?.map((v) => v.toJson()).toList();
    }
    map['favorite'] = favorite;
    map['pic'] = pic;
    map['publishTime'] = publishTime;
    map['domain'] = domain;
    map['totalAmount'] = totalAmount;
    map['userName'] = userName;
    map['country'] = country;
    map['title'] = title;
    map['pageView'] = pageView;
    map['content'] = content;
    map['thumbUp'] = thumbUp;
    map['blogType'] = blogType;
    if (blogTypeCount != null) {
      map['blogTypeCount'] = blogTypeCount?.map((v) => v.toJson()).toList();
    }
    map['top'] = top;
    map['forceUnLock'] = forceUnLock;
    return map;
  }
}
