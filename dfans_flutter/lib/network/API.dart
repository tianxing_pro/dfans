import 'package:dfans_flutter/models/CollectionShop.dart';
import 'package:dfans_flutter/models/PostsData.dart';
import 'package:dfans_flutter/models/ResponseModel.dart';
import 'package:dfans_flutter/models/User.dart';
import 'package:dfans_flutter/network/http_request.dart';

typedef RequestCallBack<T> = void Function(T value);

enum ENV { dev, test, product }

class API {
  static final API _singleton = API();
  static API getInstance() {
    _singleton._init();
    return _singleton;
  }

  String _baseURL = 'http://dfans.myslash.co:8086/';
  late HttpRequest _request;
  ENV env = ENV.dev;
  _init() {
    _request = HttpRequest(switchEnvConf(ENV.dev));
  }

  switchEnvConf(ENV env) {
    this.env = env;
    switch (env) {
      case ENV.dev:
        _baseURL = 'http://dfans.myslash.co:8086/';
        return _baseURL;
      case ENV.test:
        _baseURL = 'http://dfans.myslash.co:8086/';
        return _baseURL;
      case ENV.product:
        _baseURL = 'https://dfans.xyz/';
        return _baseURL;
      default:
        _baseURL = 'http://dfans.myslash.co:8086/';
        return _baseURL;
    }
  }

  // Future<dynamic> _getQuery(String uri, String value) async {
  //   final result = await _request
  //       .get('$uri$value?apikey=0b2bdeda43b5688921839c8ecb20399b');
  //   return result;
  // }

  Future<ResponseModel> _postQuery(String uri, dynamic body) async {
    final result = await _request.post(uri, body);
    return ResponseModel.fromJson(result);
  }

  // Future<ResponseModel> _getQuery(String uri, String? body) async {
  //   final result = await _request.get(uri, body);
  //   return ResponseModel.fromJson(result);
  // }

  void getHome(
      {List<int>? types,
      int? pages,
      int? size,
      RequestCallBack<PostsData?>? requestCallBack}) async {
    var result = await _postQuery(RequestUrl.home, {
      // 'current': 1,
      "pages": pages ?? 1,
      "size": size ?? 20,
      "tagIds": [],
      "types": types ?? [1],
      "albumId": null,
      "userCode": null
    });
    if (requestCallBack != null) requestCallBack(result.data);
  }

  void getUserInfo(
      {String? name, RequestCallBack<User?>? requestCallBack}) async {
    var result = await _request.get(RequestUrl.user, name);
    if (requestCallBack != null) requestCallBack(User.fromJson(result['data']));
  }

  void getCollectionShop(
      {String? shop_id,
      RequestCallBack<List<CollectionShop>?>? requestCallBack}) async {
    var result =
        await _request.get(RequestUrl.collectionsShop, 'SHOP_$shop_id');
    if (requestCallBack != null) {
      print('getCollectionShop ${result?['data']}');
      if (result?['data'] != null) {
        List<CollectionShop> shops = [];
        result?['data'].forEach((v) {
          shops.add(CollectionShop.fromJson(v));
        });
        requestCallBack(shops);
        // result?['data']?.map((v) {
        //   return CollectionShop.fromJson(v);
        // })
      }
    }
  }

  void fetchLogin(String? address, String? signature,
      RequestCallBack<dynamic>? requestCallBack) async {
    // // 先获取随机数
    // var nonce = await _request.get(RequestUrl.nonce, address);
    // print('- nonce - $nonce');
    // 然后调用登录接口
    final result = await _request.post(RequestUrl.login, {
      "address": address,
      "signature": signature,
    });
    print('address$address --- signature$signature -- result$result');
    if (requestCallBack != null) requestCallBack(result);
  }
}

// 请求拼接 url
class RequestUrl {
  static const home = 'api/blog/v1/home/query';
  static const user = 'api/actor/v1/home/profile/';
  static const collectionsShop = 'api/nft/v1/list/collection/';
  static const login = '/api/dfans/v1/global/login';
  static const nonce = '/api/wallet/v1/nonce/'; // 获取随机数
}
