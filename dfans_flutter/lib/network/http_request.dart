import 'dart:async';
import 'dart:convert' as Convert;
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

typedef RequestCallBack = void Function(Map data);

class HttpRequest {
  static requestGET(
      String authority, String unencodedPath, RequestCallBack callBack,
      [Map<String, String>? queryParameters]) async {
    try {
      var httpClient = HttpClient();
      var uri = Uri.http(authority, unencodedPath, queryParameters);
      var request = await httpClient.getUrl(uri);
      var response = await request.close();
      var responseBody = await response.transform(Convert.utf8.decoder).join();
      Map data = Convert.jsonDecode(responseBody);
      callBack(data);
    } on Exception catch (e) {
      print(e.toString());
    }
  }

  final baseUrl;
  final dio = Dio();

  HttpRequest(this.baseUrl);

  Future<dynamic> get(String uri, String? body,
      {Map<String, String>? headers}) async {
    try {
      final response = await dio.get(baseUrl + uri + body);
      // http.Response response =
      //     await http.post(url, body: body, headers: headers, encoding: );
      // final statusCode = response.statusCode;
      // final responseBody = response.body;
      // var result = Convert.jsonDecode(responseBody);
      // print('[uri=$uri][statusCode=$statusCode][response=$responseBody]');
      return response.data;
    } on Exception catch (e) {
      print('[uri=$uri]exception e=${e.toString()}');
      return '';
    }
  }

  // Future<dynamic> get(String uri, {Map<String, String>? headers}) async {
  //   try {
  //     http.Response response = await http.get(baseUrl + uri, headers: headers);
  //     final statusCode = response.statusCode;
  //     final body = response.body;
  //     print('[uri=$uri][statusCode=$statusCode][response=$body]');
  //     var result = Convert.jsonDecode(body);
  //     return result;
  //   } on Exception catch (e) {
  //     print('[uri=$uri]exception e=${e.toString()}');
  //     return '';
  //   }
  // }

  Future<dynamic> getResponseBody(String uri,
      {Map<String, String>? headers}) async {
    try {
      http.Response response = await http.get(baseUrl + uri, headers: headers);
      final statusCode = response.statusCode;
      final body = response.body;
//      var result = Convert.jsonDecode(body);
      print('[uri=$uri][statusCode=$statusCode][response=$body]');
      return body;
    } on Exception catch (e) {
      print('[uri=$uri]exception e=${e.toString()}');
      return null;
    }
  }

  Future<dynamic> post(String uri, dynamic body,
      {Map<String, String>? headers}) async {
    try {
      print(' +++++ boby $body');
      final response = await dio.post(baseUrl + uri, data: body);
      // final url = Uri.parse(baseUrl + uri);
      // http.Response response =
      //     await http.post(url, body: body, headers: headers, encoding: );
      // print(" --- $response");
      // final statusCode = response.statusCode;
      // final responseBody = response.body;
      // var result = Convert.jsonDecode(responseBody);
      // print('[uri=$uri][statusCode=$statusCode][response=$responseBody]');
      return response.data;
    } on Exception catch (e) {
      print('[uri=$uri]exception e=${e.toString()}');
      return '';
    }
  }
}
