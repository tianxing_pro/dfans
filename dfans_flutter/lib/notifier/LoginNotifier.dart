import 'package:dfans_flutter/notifier/DFChangeNotifier.dart';

class LoginNotifier extends DFChangeNotifier {
  String? token;

  updateToken(String? token) {
    this.token = token;
    notifyListeners();
  }
}
