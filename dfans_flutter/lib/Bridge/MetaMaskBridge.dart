import 'dart:async';
import 'package:flutter/services.dart';

class MetaMaskBridge {
  static const platform = MethodChannel('dfans.flutter.bridge');

  static final MetaMaskBridge _singleton = MetaMaskBridge();
  static MetaMaskBridge getInstance() {
    _singleton._init();
    return _singleton;
  }

  _init() {
// 注册方法处理程序
    platform.setMethodCallHandler((call) async {
      if (call.method == 'loginWithAccount') {
        // 在此处实现来自原生代码的方法
        // 可以执行相关操作并返回结果给 Flutter

        print(' call.arguments ---- ${call.arguments}');

        /// 获取到native 发送的信息
        ///

        return 'Result from native method';
      } else {
        return null;
      }
    });
  }

  Future<dynamic> getAccount() async {
    return await platform.invokeMethod('getAccount');
  }

  // static Future<T> loginWithAccount<T>(dynamic params) async {
  //   return await Bridge.callNativeStatic(
  //       "MetaMaskBridge", "loginWithAccount", params);
  // }

  // static Future<T> getAccount<T>() async {
  //   return await Bridge.callNativeStatic("MetaMaskBridge", "getAccount");
  // }

  // @override
  // String getPluginName() {
  //   return 'MetaMaskBridge';
  // }
}
