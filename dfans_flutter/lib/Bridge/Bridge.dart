import 'package:flutter/services.dart';

typedef NativeToDartMethodCallCallBack = Future<dynamic> Function(
    String methodName, dynamic arguments);

abstract class Bridge {
  static const String CHANNEM_NAME = "dfans.flutter.bridge/callNative";

  static MethodChannel? _globalBridgeMethodChannel;
  static final Map<String, NativeToDartMethodCallCallBack>
      _globalNativeToDartMethodMap = {};

  static void registerMethodCallBack(
      String methodName, NativeToDartMethodCallCallBack callBack) {
    _initGlobalBridgeMethodChannel();
    _globalNativeToDartMethodMap[methodName] = callBack;
  }

  static _initGlobalBridgeMethodChannel() {
    if (_globalBridgeMethodChannel == null) {
      _globalBridgeMethodChannel =
          const MethodChannel(CHANNEM_NAME, JSONMethodCodec());
      _globalBridgeMethodChannel?.setMethodCallHandler((MethodCall call) {
        return _invokeNativeToDartMethod(call.method, call.arguments);
      });
    }
  }

  static Future<dynamic> _invokeNativeToDartMethod(
      String methodName, dynamic arguments) {
    if (_globalNativeToDartMethodMap.containsKey(methodName)) {
      return _globalNativeToDartMethodMap[methodName]!(methodName, arguments);
    }
    return Future<dynamic>(
      () {},
    );
  }

  static void unregisterMethodCallBack(String methodName) {
    _globalNativeToDartMethodMap.remove(methodName);
  }

  /// 调用Native Plugin方法
  static Future<T> callNativeStatic<T>(String pluginName, String methodName,
      [dynamic arguments]) async {
    _initGlobalBridgeMethodChannel();
    return await _globalBridgeMethodChannel?.invokeMethod(
        "$pluginName-$methodName", arguments);
  }

  /// 调用Native Plugin方法
  Future<T> callNative<T>(String methodName, [dynamic arguments]) async {
    return await callNativeStatic(getPluginName(), methodName, arguments);
  }

  /// plugin名字
  String getPluginName();
}
