import MetaMaskSDK from '@metamask/sdk';
import { Linking } from 'react-native';
import BackgroundTimer from 'react-native-background-timer';
import { ethers } from 'ethers';

// import { Core } from '@walletconnect/core';
// // import { ICore } from '@walletconnect/types' <- Add if using TS
// import { Web3Wallet } from '@walletconnect/web3wallet';

// const core = new Core({
//   projectId: process.env.PROJECT_ID,
// });

// export async function createWeb3Wallet() {
//   const web3wallet = await Web3Wallet.init({
//     core, // <- pass the shared `core` instance
//     metadata: {
//       name: 'Demo React Native Wallet',
//       description: 'Demo RN Wallet to interface with Dapps',
//       url: 'www.walletconnect.com',
//       icons: [],
//     },
//   });
// }
// export async function pair(uri) {
//   return await core.pairing.pair({ uri: params.uri });
// }

const MMSDK = new MetaMaskSDK({
  openDeeplink: link => {
    Linking.openURL(link); // Use React Native Linking method or another way of opening deeplinks.
  },
  useDeeplink: true,
  timer: BackgroundTimer, // To keep the dapp alive once it goes to background.
  dappMetadata: {
    name: 'My dapp', // The name of your dapp.
    url: 'https://eth-mainnet.g.alchemy.com/v2/_MiMgyxpL18p9m6mEPG2Q8-oHK6ydxHI', // The URL of your website.
  },
});

const ethereum = MMSDK.getProvider();

export const connect = async callBack => {
  try {
    const result = await ethereum?.request({ method: 'eth_requestAccounts' });
    console.log('RESULT', result);
    callBack?.(result);
  } catch (e) {
    callBack?.(e);
    console.log('ERROR', e);
  }
};
