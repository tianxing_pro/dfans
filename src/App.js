import React, { memo, useState, useEffect } from 'react';
import {
  Image,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  View,
  Text,
} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import MetaMaskSDK from '@metamask/sdk';
import { Linking, NativeModules } from 'react-native';
import BackgroundTimer from 'react-native-background-timer';
import LinearGradient from 'react-native-linear-gradient';
// import Http from '@tradle/react-native-http';

const MMSDK = new MetaMaskSDK({
  openDeeplink: link => {
    Linking.openURL(link); // Use React Native Linking method or another way of opening deeplinks.
  },
  useDeeplink: true,
  timer: BackgroundTimer, // To keep the dapp alive once it goes to background.
  dappMetadata: {
    name: 'My dapp', // The name of your dapp.
    url: 'https://eth-mainnet.g.alchemy.com/v2/_MiMgyxpL18p9m6mEPG2Q8-oHK6ydxHI', // The URL of your website.
  },
});

const ethereum = MMSDK.getProvider();

const screenHeight = Dimensions.get('window').height;

const App = () => {
  const isDarkMode = false; //useColorScheme() === 'dark';

  const [account, setAccount] = useState();

  const backgroundStyle = { backgroundColor: isDarkMode ? Colors.darker : Colors.lighter };

  useEffect(() => {
    // 链接的链改变
    // ethereum.on('chainChanged', chain => {
    //   console.log(chain);
    //   setChain(chain);
    // });
    // deepLink 回调事件, 授权状态改变
    ethereum.on('accountsChanged', async accounts => {
      console.log(`accounts:${accounts}`);
      setAccount(accounts?.[0]);
      setTimeout(async () => {
        await onSign(accounts?.[0]);
      }, 500);
    });
  }, []);

  // 另一种授权方式
  const signPerson = async account => {
    const from = account;
    const msgParams = [
      `Welcome to dFans! Please sign this message to log in.`,
      ,
      'Signing is the only way we can truly know that you are the owner of the wallet. Signing does not in any way perform any transactions with your wallet.',
      ,
      'Nonce:',
      from + '610531',
    ].join('\n');
    try {
      // For historical reasons, you must submit the message to sign in hex-encoded UTF-8.
      // This uses a Node.js-style buffer shim in the browser.
      // // const random = await axios.getUri(`http://dfans.myslash.co:8086/api/wallet/v1/nonce/${random}`);
      // const name = 'dFans';

      const msg = `0x${Buffer.from(msgParams, 'utf8').toString('hex')}`;
      const signature = await ethereum.request({
        method: 'personal_sign',
        params: [msg, from],
      });
      console.log('signature', signature);
      NativeModules.RNBridge.closeLoginPage({ account: account, signature: signature }, () => {});
    } catch (err) {
      console.log(`error ---- ${err}`);
    }
  };

  // 授权 MetaMask
  const connect = async () => {
    try {
      const result = await ethereum.request({ method: 'eth_requestAccounts' });
      console.log('RESULT', result?.[0]);
      setAccount(result?.[0]);
    } catch (e) {
      console.log('ERROR', e);
    }
  };

  // v4 签名
  const onSign = async account => {
    const msgParams = JSON.stringify({
      domain: {
        // Defining the chain aka Rinkeby testnet or Ethereum Main Net
        chainId: parseInt(ethereum.chainId, 16),
        // Give a user friendly name to the specific contract you are signing for.
        name: 'Ether Mail',
        // If name isn't enough add verifying contract to make sure you are establishing contracts with the proper entity
        verifyingContract: '0xCcCCccccCCCCcCCCCCCcCcCccCcCCCcCcccccccC',
        // Just let's you know the latest version. Definitely make sure the field name is correct.
        version: '1',
      },

      // Defining the message signing data content.
      message: {
        /*
         - Anything you want. Just a JSON Blob that encodes the data you want to send
         - No required fields
         - This is DApp Specific
         - Be as explicit as possible when building out the message schema.
        */
        contents: 'Hello, Bob!',
        attachedMoneyInEth: 4.2,
        from: {
          name: 'Cow',
          wallets: [
            '0xCD2a3d9F938E13CD947Ec05AbC7FE734Df8DD826',
            '0xDeaDbeefdEAdbeefdEadbEEFdeadbeEFdEaDbeeF',
          ],
        },
        to: [
          {
            name: 'Bob',
            wallets: [
              '0xbBbBBBBbbBBBbbbBbbBbbbbBBbBbbbbBbBbbBBbB',
              '0xB0BdaBea57B0BDABeA57b0bdABEA57b0BDabEa57',
              '0xB0B0b0b0b0b0B000000000000000000000000000',
            ],
          },
        ],
      },
      // Refers to the keys of the *types* object below.
      primaryType: 'Mail',
      types: {
        // TODO: Clarify if EIP712Domain refers to the domain the contract is hosted on
        EIP712Domain: [
          { name: 'name', type: 'string' },
          { name: 'version', type: 'string' },
          { name: 'chainId', type: 'uint256' },
          { name: 'verifyingContract', type: 'address' },
        ],
        // Not an EIP712Domain definition
        Group: [
          { name: 'name', type: 'string' },
          { name: 'members', type: 'Person[]' },
        ],
        // Refer to PrimaryType
        Mail: [
          { name: 'from', type: 'Person' },
          { name: 'to', type: 'Person[]' },
          { name: 'contents', type: 'string' },
        ],
        // Not an EIP712Domain definition
        Person: [
          { name: 'name', type: 'string' },
          { name: 'wallets', type: 'address[]' },
        ],
      },
    });
    var from = ethereum.selectedAddress;
    var params = [from, msgParams];
    var method = 'eth_signTypedData_v4';
    const signature = await ethereum.request({ method, params });
    console.log('signature', signature);
    NativeModules.RNBridge.closeLoginPage({ account: account, signature: signature }, () => {});
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar
        translucent={true}
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={'#fff'}
      />
      <LinearGradient
        colors={['rgba(252, 213, 53, 0.1)', 'rgba(252, 213, 53, 0)']} // 渐变色数组，可以定义多个颜色
        start={{ x: 0, y: 0 }} // 渐变的起始点，从左上角开始
        end={{ x: 0, y: 1 }} // 渐变的终止点，到右下角结束
        style={{ flex: 1 }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <Image
            source={require('./assets/images/theme.png')}
            resizeMode={'stretch'}
            style={{
              resizeMode: 'contain',
              marginBottom: 80,
            }}
          />
          <View style={{ justifyContent: 'center' }}>
            <TouchableOpacity onPress={connect}>
              <Image
                source={require('./assets/images/MetaMask.png')}
                resizeMode={'stretch'}
                style={{
                  resizeMode: 'contain',
                }}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{ marginTop: 50 }} onPress={onSign}>
              <Image
                source={require('./assets/images/WalletConnect.png')}
                resizeMode={'stretch'}
                style={{
                  resizeMode: 'contain',
                }}
              />
            </TouchableOpacity>
            <Text>{account && `Connected account: ${account}\n\n`}</Text>
          </View>
        </View>
      </LinearGradient>
    </SafeAreaView>
  );
};

export default memo(App);
