module.exports = {
  root: true,
  extends: ['@react-native-community', 'prettier'],
  rules: {
    /**
     * must disable the base rule as it can report incorrect errors.
     * @link https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-use-before-define.md#how-to-use
     */
    'no-use-before-define': 'off',
    'react-hooks/exhaustive-deps': 'warn',
    'react-hooks/rules-of-hooks': 'error',
    'react-native/no-inline-styles': ['warn', { allowStylePropertiesLessThan: 4 }],
    'no-shadow': 'off',
    'eslint-disable react-native/no-inline-styles': true,
  },
};
