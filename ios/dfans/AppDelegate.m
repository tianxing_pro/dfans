#import "AppDelegate.h"
#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import "LoginViewController.h"
#import <FlutterPluginRegistrant/GeneratedPluginRegistrant.h>
#import "FlutterChannelManager.h"
#import "FlutterRootViewController.h"
#import "Constants.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  self.flutterEngine = [[FlutterEngine alloc] initWithName:@"MyApp"];
  [self.flutterEngine run];
  [GeneratedPluginRegistrant registerWithRegistry:self.flutterEngine];
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  NSDictionary *params = [[NSUserDefaults standardUserDefaults] valueForKey:LoginWithAccount];
  if (params == nil) {
    LoginViewController *rootViewController = [[LoginViewController alloc] init];
    self.window.rootViewController = rootViewController;
    [self.window makeKeyAndVisible];
  }else{
    FlutterViewController *vc = [[FlutterViewController alloc] initWithEngine:self.flutterEngine nibName:nil bundle:nil];
    [[FlutterChannelManager sharedManager] registerChannel:self.flutterEngine];
    [[FlutterChannelManager sharedManager] loginWithAccount:params];
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];
  }
  return YES;
}

@end

