
#import <UIKit/UIKit.h>

@import Flutter;

@interface AppDelegate : FlutterAppDelegate <UIApplicationDelegate>

@property (nonatomic, strong) FlutterEngine *flutterEngine;

@end
