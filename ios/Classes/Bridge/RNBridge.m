//
//  RNBridge.m
//  dfans
//
//  Created by delin on 2023/7/18.
//

#import "RNBridge.h"
#import "LoginViewController.h"
#import "FlutterRootViewController.h"
#import "FlutterChannelManager.h"
#import "Constants.h"


@implementation RNBridge

RCT_EXPORT_MODULE(RNBridge);


RCT_EXPORT_METHOD(closeLoginPage:(NSDictionary *)params call:(RCTResponseSenderBlock)callback){

  dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@" params ----- %@", params);
     [[NSUserDefaults standardUserDefaults] setValue:params forKey:LoginWithAccount];
     [[NSUserDefaults standardUserDefaults] synchronize];
    UIWindow* window = [[UIApplication sharedApplication] delegate].window;
    if ([window.rootViewController isKindOfClass:[FlutterViewController class]]) {
      [window.rootViewController dismissViewControllerAnimated:true completion:^{}];
      return;
    }
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    FlutterEngine *flutterEngine = appDelegate.flutterEngine;
    [[FlutterChannelManager sharedManager] registerChannel: flutterEngine];
    [[FlutterChannelManager sharedManager] loginWithAccount:params];
    FlutterViewController *vc = [[FlutterViewController alloc] initWithEngine:flutterEngine nibName:nil bundle:nil];
    window.rootViewController = vc;
    [window makeKeyAndVisible];
   
  });
}

@end
