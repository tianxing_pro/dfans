//
//  FlutterChannelManager.h
//  dfans
//
//  Created by delin on 2023/7/19.
//

#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>

@interface FlutterChannelManager : NSObject

@property(nonatomic, strong) FlutterMethodChannel *methodChannel;

+ (instancetype)sharedManager;

- (void)registerChannel:(FlutterEngine *)flutterEngine;

- (void)callFlutterMethod;

- (void)loginWithAccount: (NSDictionary *)account;


@end


