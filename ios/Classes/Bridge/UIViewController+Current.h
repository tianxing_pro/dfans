//
//  UIViewController+Current.h
//  BlackLakePrint
//
//  Created by delin on 2023/1/4.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UIViewController (Current)

+ (UIViewController *)currentViewController;

@end
