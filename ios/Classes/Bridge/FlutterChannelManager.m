//
//  FlutterChannelManager.m
//  dfans
//
//  Created by delin on 2023/7/19.
//

#import "FlutterChannelManager.h"
#import "LoginViewController.h"
#import "UIViewController+Current.h"
#import "Constants.h"

@interface FlutterChannelManager()

@end


@implementation FlutterChannelManager

+ (instancetype)sharedManager {
    static FlutterChannelManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}


- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)registerChannel:(FlutterEngine *)flutterEngine {
  self.methodChannel = [FlutterMethodChannel methodChannelWithName:@"dfans.flutter.bridge"
                                              binaryMessenger:[flutterEngine binaryMessenger]];
  [self.methodChannel setMethodCallHandler:^(FlutterMethodCall *call, FlutterResult result) {
    if ([call.method isEqualToString:@"getAccount"]) {
      NSDictionary *params = [[NSUserDefaults standardUserDefaults] valueForKey:LoginWithAccount];
      result(params);
    }else{
      result(FlutterMethodNotImplemented);
    }
  }];
}

- (void)callFlutterMethod {
    // 调用 Flutter 方法
    [self.methodChannel invokeMethod:@"flutterMethod" arguments:nil];
}

- (void)loginWithAccount: (NSDictionary *)account {
  // 调用 Flutter 方法
  [self.methodChannel invokeMethod:@"loginWithAccount" arguments:account];
}

@end

