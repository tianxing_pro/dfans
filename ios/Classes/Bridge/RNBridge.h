//
//  RNBridge.h
//  dfans
//
//  Created by delin on 2023/7/18.
//

#import <Foundation/Foundation.h>
#import "UIViewController+Current.h"
#import <React/RCTBridge.h>

NS_ASSUME_NONNULL_BEGIN

@interface RNBridge : NSObject<RCTBridgeModule>


@end

NS_ASSUME_NONNULL_END
