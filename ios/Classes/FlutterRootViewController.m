//
//  FlutterRootViewController.m
//  dfans
//
//  Created by delin on 2023/7/9.
//

#import "FlutterRootViewController.h"
#import "AppDelegate.h"
@import Flutter;

@interface FlutterRootViewController ()

@end

@implementation FlutterRootViewController

- (void)loadView {
  AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
  FlutterEngine *flutterEngine = appDelegate.flutterEngine;
  FlutterViewController *vc = [[FlutterViewController alloc] initWithEngine:flutterEngine nibName:nil bundle:nil];
  self.view = vc.view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

@end
