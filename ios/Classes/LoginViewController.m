//
//  LoginViewController.m
//  dfans
//
//  Created by delin on 2023/7/9.
//

#import "LoginViewController.h"
#import <React/RCTBridgeDelegate.h>
#import <React/RCTBridge.h>
#import <React/RCTRootView.h>
#import <React/RCTBundleURLProvider.h>

@interface LoginViewController ()<RCTBridgeDelegate>

@end

@implementation LoginViewController

- (void)loadView {
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:nil];
       RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                        moduleName:@"dfans"
                                                 initialProperties:nil];
      rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
      if (@available(iOS 13.0, *)) {
        rootView.backgroundColor = [UIColor systemBackgroundColor];
      } else {
        rootView.backgroundColor = [UIColor whiteColor];
      }
      self.view = rootView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}

@end
