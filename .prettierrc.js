module.exports = {
  printWidth: 100,
  tabWidth: 2,
  bracketSpacing: true,
  bracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  arrowParens: 'avoid',
};
